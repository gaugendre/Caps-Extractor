# Caps Extractor
This program is distributed under the GNU GPL v3. Please quote Gabriel Augendre \<gabriel&nbsp;[at]&nbsp;augendre.info\> as the original author.

_Caps Extractor_ is a simple Java app designed to extract upper case characters from a string and prompt them to the user.
You also have the possibility to copy the generated acronym to the clipboard for reuse.

# Reuse
If you do reuse my work, please consider linking back to this repository 🙂